# Fletchify

This is a strange little web-based GIF generator for custom Murder, She Wrote title cards.

It creates things like this:

![An example title card showing the caption Welcome to Hell in front of crashing waves.](msw-welcome-to-hell.gif)

Enjoy!
