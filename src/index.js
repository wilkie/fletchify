import React from 'react';
import ReactDOM from 'react-dom';

import FFmpeg from '../node_modules/@ffmpeg/ffmpeg/dist/ffmpeg.min.js';

// Stylesheets
import './index.scss';

// Background videos
import videoWaves from './assets/video/intro-waves.320.mp4'
import videoWavesImage from './assets/frames/intro-waves.320.png'

import videoForest from './assets/video/intro-forest.320.mp4'
import videoForestImage from './assets/frames/intro-forest.320.png'

import videoStreet from './assets/video/intro-street.320.mp4'
import videoStreetImage from './assets/frames/intro-street.320.png'

// Acquire an instance of ffmpeg (and necessary libraries)
const { createFFmpeg, fetchFile } = FFmpeg;
const ffmpeg = createFFmpeg({log: true});

const font = "30pt \"Lansbury\"";

const delay = 45;

function string_to_slug (str) {
    str = str.replace("\n", "-");
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
  
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

/**
 * Measures text.
 */
var measureText = function(ctx, font, text) {
    var textMetrics = ctx.measureText(text);

    var fontCheck = font.replace(new RegExp("\"", 'g'), "")

    var measureDiv = document.querySelector(".textMeasure[data-font=\"" + fontCheck + "\"]");

    if (!measureDiv) {
        var textElement = document.createElement("span");
        textElement.textContent = "Hg";
        textElement.style.font = font;
        var block = document.createElement("div");
        block.style.display = "inline-block";
        block.style.width = "1px";
        block.style.height = "0px";

        var div = document.createElement("div");
        div.classList.add("textMeasure");
        div.setAttribute("data-font", fontCheck);
        div.style.position = 'absolute';
        div.style.opacity = 0;
        div.appendChild(textElement);
        div.appendChild(block);
        document.body.appendChild(div);

        block.style.verticalAlign = "baseline";
        var blockRect = block.getBoundingClientRect();
        var textRect  = textElement.getBoundingClientRect()

        var textAscent = blockRect.top - textRect.top;

        block.style.verticalAlign = "bottom";
        blockRect = block.getBoundingClientRect();
        textRect  = textElement.getBoundingClientRect()

        var textHeight = blockRect.top - textRect.top;
        var textDescent = textHeight - textAscent;

        div.setAttribute("data-text-ascent",  textAscent);
        div.setAttribute("data-text-height",  textHeight);
        div.setAttribute("data-text-descent", textDescent);

        measureDiv = div;
    }

    textMetrics.height  = parseInt(measureDiv.getAttribute("data-text-height"));
    textMetrics.ascent  = parseInt(measureDiv.getAttribute("data-text-ascent"));
    textMetrics.descent = parseInt(measureDiv.getAttribute("data-text-descent"));

    return textMetrics;
};

function createGIF(button, video, caption) {
    button.setAttribute('disabled', '');
    button.textContent = "0%";

    (async () => {
        // Initialize ffmpeg
        if (!ffmpeg.isLoaded()) {
            await ffmpeg.load();
        }

        button.textContent = "10%";

        // Write the video to internal memory
        const name = "input.mp4";
        ffmpeg.FS('writeFile', name, await fetchFile(video));

        // Generate each frame
        await ffmpeg.run('-y', '-i', name, '-vf', 'fps=15', 'frame-%05d.png');
        const frames = ffmpeg.FS('readdir', '/');

        button.textContent = "20%";

        // Create the caption on most of the frames
        let index = 0;

        const blobs = [];

        // Filter to only images in frames list
        let fileList = [];
        for (let filename of frames) {
            if (filename.endsWith(".png")) {
                fileList.push(filename);
            }
        }

        const images = {};
        const canvases = {};

        for (let filename of fileList) {
            if (index >= delay) {
                console.log("FRAME", index, filename);

                images[index] = document.createElement("img");
                canvases[index] = document.createElement("canvas");

                images[index].setAttribute('data-index', index);

                images[index].addEventListener("load", async (event) => {
                    let index = parseInt(event.target.getAttribute('data-index'));
                    var imageWidth  = images[index].clientWidth;
                    var imageHeight = images[index].clientHeight;

                    canvases[index].width  = imageWidth;
                    canvases[index].height = imageHeight;

                    canvases[index].setAttribute('width', imageWidth);
                    canvases[index].setAttribute('height', imageHeight);

                    var ctx = canvases[index].getContext('2d');
                    ctx.drawImage(images[index], 0, 0);
                    ctx.textAlign = "left";
                    ctx.textBaseline = "top";
                    ctx.font = font;

                    var lines = caption.split("\n");
                    lines.forEach( (text, i) => {
                        var x, y;
                        var textMetrics = measureText(ctx, font, text);
                        console.log(textMetrics);
                        x = (imageWidth / 2) - (textMetrics.width / 2);
                        y = (imageHeight / 2) - (textMetrics.height * lines.length / 2) + (textMetrics.height * i) + 5;
                        console.log("writing line", text, i, y);

                        ctx.fillStyle = "#000";
                        ctx.fillText(text, x+2, y+2);

                        ctx.fillStyle = "#f9fb61";
                        ctx.strokeStyle = "#000";
                        ctx.strokeText(text, x, y);
                        ctx.fillText(text, x, y);
                    });

                    const blob = await new Promise(resolve => canvases[index].toBlob(resolve));

                    console.log(canvases[index], blob);

                    blobs.push({
                        "name": filename,
                        "data": blob
                    });

                    // Destroy the image and the canvas
                    //image.remove();
                    //canvas.remove();

                    const total = fileList.length - delay;
                    button.textContent = Math.round(20 + (blobs.length / total) * 60) + "%";

                    if (blobs.length === (fileList.length - delay)) {
                        button.textContent = "80%";

                        // Feed resulting images back to ffmpeg
                        for (let frameData of blobs) {
                            console.log(frameData);
                            const buffer = await frameData.data.arrayBuffer();
                            console.log(buffer);
                            ffmpeg.FS('writeFile', frameData.name, new Uint8Array(buffer));
                        }

                        // Create new video
                        await ffmpeg.run('-y', '-i', "frame-%05d.png", "-vf", "fps=15", "video.mp4");

                        // Create the palette
                        console.log("CREATING PALETTE");
                        await ffmpeg.run('-y', '-i', "video.mp4", "-vf", "fps=15,palettegen", "palette.png");
                        console.log("CREATING GIF");
                        await ffmpeg.run('-y', '-i', "video.mp4", "-i", "palette.png", "-filter_complex", "[0:v][1:v] paletteuse", "output.gif");

                        const data = ffmpeg.FS('readFile', 'output.gif');

                        const url = URL.createObjectURL(new Blob([data.buffer], { type: 'image/gif' }));

                        const a = document.createElement('a');
                        a.href = url;
                        a.download = "msw-" + string_to_slug(caption);
                        a.click();

                        button.textContent = "Render";
                        button.removeAttribute('disabled');
                    }
                });

                var pngData = ffmpeg.FS('readFile', filename);
                var pngBlob = new Blob([pngData], {'type': 'image/png'});
                var png = null;

                if (pngBlob) {
                    png = (URL || window.webkitURL).createObjectURL(pngBlob);
                }
                else {
                    var pngB64 = btoa(String.fromCharCode.apply(null, pngData));
                    png = 'data:image/png;base64,' + pngB64;
                }

                var scratch = document.querySelector('div.scratch');
                scratch.appendChild(images[index]);
                scratch.appendChild(canvases[index]);

                images[index].src = png;
            }
            index++;
        }
    })();
}

/**
 * The video selection panel.
 */
class VideoSelector extends React.Component {
    renderVideo(video, image) {
        return <li className="video"><img alt="Ocean waves crashing on rocky beach." src={image} data-video-url={video} onClick={ (event) => this.props.onChange(event.target.getAttribute('data-video-url')) } /></li>
    }

    render() {
        return (
            <ul>
                {this.renderVideo(videoWaves, videoWavesImage)}
                {this.renderVideo(videoForest, videoForestImage)}
                {this.renderVideo(videoStreet, videoStreetImage)}
            </ul>
        );
    }
}

/**
 * The preview panel.
 */
class VideoPreview extends React.Component {
    render() {
        const textItems = [];

        const lines = this.props.caption.split("\n");

        const lineHeight = 45;

        // Center
        let y = 150 + lineHeight;

        // Height
        const height = lines.length * lineHeight;
        y = y - (height / 2);

        lines.forEach( (line) => {
            textItems.push(<text x="50%" y={y} text-anchor="middle">{line}</text>)

            y += lineHeight;
        });

        return <div className="preview"><img role="presentation" alt="" src="placeholder.png"/><video key={this.props.video} autoPlay={true} muted={true} playsInline={true} loop={true}><source src={this.props.video} type="video/mp4"/></video><svg className="caption" viewBox="0 0 400 300">{textItems}</svg><img alt="The resulting GIF." className="rendered" id="result" hidden={true} /></div>
    }
}

/**
 * The video caption editing panel.
 */
class VideoCaption extends React.Component {
    render() {
        return (
            <div className="video-caption">
                <textarea onChange={(event) => this.props.onChange(event.target.value)}>{this.props.caption}</textarea>
                <div className="actions">
                    <button onClick={(event) => {createGIF(event.target, this.props.video, this.props.caption)}}>Render</button>
                </div>
            </div>
        );
    }
}

/**
 * Main application.
 */
class Fletchify extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            video: {videoWaves}.videoWaves,
            caption: "Welcome to\nHell"
        };
    }

    render() {
        return (
            <div className="app">
                <div className="video-selector-panel">
                    <VideoSelector onChange={ (url) => this.setState({video: url}) } />
                </div>
                <div>
                    <div className="video-preview">
                         <VideoPreview caption={this.state.caption} video={this.state.video} />
                    </div>
                    <VideoCaption caption={this.state.caption} video={this.state.video} onChange={ (text) => this.setState({caption: text}) } />
                </div>
            </div>
        );
    }
}

// Main entrypoint
ReactDOM.render(
    <Fletchify />,
    document.getElementById('root')
);
