mkdir -p src/assets/frames

FILES=$(ls src/assets/video/*.mp4)

for f in $FILES
do
  b="$(basename -- $f)"
  t="${b%.mp4}"

  echo "Processing $t file..."

  # Get the 60th frame
  ffmpeg -y -i $f -vf "select=eq(n\,59)" -vframes 1 src/assets/frames/$t.png
done
